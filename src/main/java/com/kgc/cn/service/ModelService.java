package com.kgc.cn.service;

import com.kgc.cn.model.Model;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * created on 2020/1/3 0003.
 */
@FeignClient(name = "service-a",fallback = ModelService.showB.class )
public interface ModelService {

    @PostMapping(value = "/test/model")
    Model toshow(@RequestBody Model model);

    @Component
     class showB implements ModelService{

        @Override
        public Model toshow(@RequestBody Model model) {
            return new Model();
        }
    }
}
