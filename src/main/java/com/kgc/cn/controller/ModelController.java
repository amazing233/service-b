package com.kgc.cn.controller;

import com.kgc.cn.model.Model;
import com.kgc.cn.service.ModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * created on 2020/1/3 0003.
 */
@RestController
@RequestMapping("/web")
public class ModelController {

    @Autowired
    private ModelService modelService;

    @PostMapping("/test")
    public Model toshow(@Valid Model model){
        return modelService.toshow(model);
    }
}
