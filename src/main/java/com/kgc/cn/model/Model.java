package com.kgc.cn.model;

import java.io.Serializable;

/**
 * created on 2020/1/3 0003.
 */
public class Model implements Serializable {

    private static final long serialVersionUID = -5419223491495361260L;
    private String name;
    private String phone;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }


}
